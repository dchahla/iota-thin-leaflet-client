window.setInterval(function(){
    navigator.geolocation.getCurrentPosition(geoSuccess, geoError,   {
      enableHighAccuracy: true, 
      timeout: 3500,
      maximumAge: 10000
    });
    function geoSuccess(getCurrentPos){
        
        console.log(getCurrentPos.coords.accuracy, getCurrentPos.coords.latitude, getCurrentPos.coords.longitude, getCurrentPos.timestamp)
        fetch(`/publish?timestamp=${getCurrentPos.timestamp}&latitude=${getCurrentPos.coords.latitude}&longitude=${getCurrentPos.coords.longitude}&accuracy=${getCurrentPos.coords.accuracy}&speed=${getCurrentPos.coords.speed || 0}`)
  .then(function(response) {
    return response.json();
  })
  .then(function(myJson) {
    if(myJson.redirect){
      console.log(myJson.redirect, JSON.stringify(myJson));
      window.location.href =  myJson.redirect
    }
  })
  .catch(function(err){
    // console.log(err);
  })
    }

   function geoError (err){
        console.log(err)
    }
}, 5000)