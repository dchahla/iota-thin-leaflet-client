var express = require('express');
var router = express.Router();
var mqtt = require('mqtt')
const uuid = require('uuidv4');
const { exec } = require('child_process')



  exec(`python hx711.py
     `, (err, stdout, stderr) => {
   if (err) {
    console.log("hx711.py", err)
     // node couldn't execute the command
    // return;
   }
   console.log(`stdout: ${stdout}`);
   console.log(`stderr: ${stderr}`);
   updateFb("hx711.py", stdout)
   // the *entire* stdout and stderr (buffered)
   // console.log(`stdout: ${stdout}`);
   // console.log(`stderr: ${stderr}`);
  });

  exec(`sudo sh /etc/nds_auth/accesstoken.sh 
     `, (err, stdout, stderr) => {
   if (err) {
    updateFb("accesstoken", stdout)
     // node couldn't execute the command
    // return;
   }
   console.log(`stdout: ${stdout}`);
   console.log(`stderr: ${stderr}`);
   updateFb("accesstoken", stdout)
   // the *entire* stdout and stderr (buffered)
   // console.log(`stdout: ${stdout}`);
   // console.log(`stderr: ${stderr}`);
  });



function updateFb(titel, string){
 console.log(title, string)
}


var eddystoneBeacon = require('eddystone-beacon');
var name = uuid()
var options = {
    name: name,    // set device name when advertising (Linux only)
    txPowerLevel: -22, // override TX Power Level, default value is -21,
    tlmCount: 2,       // 2 TLM frames
    tlmPeriod: 10      // every 10 advertisements
  };
  var namespaceId = '00010203040506070809';
var instanceId = 'aabbccddeeff';

// setTimeout(function(){
    eddystoneBeacon.advertiseUid(namespaceId, instanceId, [options]);

// }, 1000)
eddystoneBeacon.advertiseUrl('http://google.com', [options]);

var batteryVoltage = 500; // between 500 and 10,000 mV

eddystoneBeacon.setBatteryVoltage(batteryVoltage);



const BeaconScanner = require('node-beacon-scanner');
const scanner = new BeaconScanner();
 
// Set an Event handler for becons
scanner.onadvertisement = (ad) => {
  console.log("Found")
  console.log(JSON.stringify(ad, null, '  '));
};
 
// Start scanning
scanner.startScan().then(() => {
  console.log('Started to scan.')  ;
}).catch((error) => {
  console.error(error);
});






var position = {}
var settings = {
  protocolId: 'MQIsdp',
  protocolVersion: 3,
  clientId: 'Thermostat1a', // This is not the Auth0 ClientID (it is the MQTT clientID)
  username: 'danny@test.com',
  password: 'asasas',
  clean: false
}

// client connection
var mqttClient = mqtt.connect(process.env.MQTT_URL, settings)
var secondselapsed = 0
setInterval(sendTemperature, 1000, mqttClient)

function sendTemperature (client) {
  secondselapsed++
  position.will =   {topic: 'lift' , qos: 0}
  position.sid = uuid()
  position.mid = name
  console.log(secondselapsed, JSON.stringify(position))

  // publish on the "temperature" topic
  client.publish('lift', JSON.stringify(position))
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/publish', function(req, res, next) {
  console.log(Number(req.query.longitude), Number(req.query.latitude) )
  position = req.query
  data.points.coordinates[0] = [Number(req.query.longitude), Number(req.query.latitude)]
  data.points.coordinates[1] = [Number(req.query.longitude) - 0.080, Number(req.query.latitude) - 0.004]
  data.points.coordinates[2] = [Number(req.query.longitude) + 0.099, Number(req.query.latitude) - 0.008]
  data.points.coordinates[4] = [Number(req.query.longitude) + 0.031, Number(req.query.latitude) - 0.0075]
  // data.lines.coordinates[0] = [Number(req.query.longitude), Number(req.query.latitude)]
  // data.lines.coordinates[1] = [Number(req.query.longitude) - 0.080, Number(req.query.latitude) - 0.004]
  // data.lines.coordinates[2] = [Number(req.query.longitude) + 0.099, Number(req.query.latitude) - 0.008]
  // data.lines.coordinates[4] = [Number(req.query.longitude) + 0.031, Number(req.query.latitude) - 0.0075]
  // data.polygons.coordinates[0] = [Number(req.query.longitude), Number(req.query.latitude)]
  // data.polygons.coordinates[1] = [Number(req.query.longitude) - 0.080, Number(req.query.latitude) - 0.004]
  // data.polygons.coordinates[2] = [Number(req.query.longitude) + 0.099, Number(req.query.latitude) - 0.008]
  // data.polygons.coordinates[4] = [Number(req.query.longitude) + 0.031, Number(req.query.latitude) - 0.0075]
  data.redirect = '/map'
  res.json(data)
});




router.get('/api/v1/command/:command', function (req, res) {
  exec(`
   python pyScanner.py   
      `, (err, stdout, stderr) => {
    if (err) {
        console.log(err)
        res.json({err: err })
      // node couldn't execute the command
      return;
    }
    console.log(`stdout: ${stdout}`);
    console.log(`stderr: ${stderr}`);
    res.json({version: 'one' ,
     stdout:stdout, 
     stderr: stderr})
    // the *entire* stdout and stderr (buffered)
    // console.log(`stdout: ${stdout}`);
    // console.log(`stderr: ${stderr}`);
  });
  })


/* GET json data. */
router.get('/mapjson/:name', function (req, res) {
  if (req.params.name) {
res.json(data[req.params.name])
  }
});

/* GET layers json data. */
router.get('/maplayers', function (req, res) {
  res.json(data);
});

/* GET Map page. */
router.get('/map', function(req,res) {

      if(position.latitude, position.longitude)

      {

        res.render('map', {
          "jmap" : data,
          lat : Number(position.latitude),
          lng : Number(position.longitude)
      });
      }
      else{
        res.redirect("/")
      }
});

var data = {"points": { 
  "type": "MultiPoint",
  "name": "points",
  "color": "#0000ff",
  "style": {
      "radius": 8,
      "fillColor": "#00ce00",
      "color": "#008c00",
      "weight": 2,
      "opacity": 1,
      "fillOpacity": 1
  },
  "coordinates": [
     [-73.9580, 40.8003 ],
     [-73.9498, 40.7968  ],
     [ -73.9737, 40.7648 ],
     [ -73.9814, 40.7681 ]
  ]
},
"lines": {
"type": "MultiLineString",
"name": "lines",
"style": {
  "color": "#ff46b5",
  "weight": 10,
  "opacity": 0.85
},
"coordinates": [
   [ [ -73.96943, 40.78519 ], [ -73.96082, 40.78095 ] ],
   [ [ -73.96415, 40.79229 ], [ -73.95544, 40.78854 ] ],
   [ [ -73.97162, 40.78205 ], [ -73.96374, 40.77715 ] ],
   [ [ -73.97880, 40.77247 ], [ -73.97036, 40.76811 ] ]
]
},
"polygons": { 
  "type": "FeatureCollection", 
   "name": "polygons", 
  "features":  
  [{ 
  "type": "Feature", 
  "properties": {"style": "Orange", "name": "Orange"}, 
  "geometry": { 
      "type": "Polygon", 
      "coordinates":  [ [  
          [  -73.9814, 40.7681 ], [ -73.958, 40.8003 ], [ -73.9737, 40.7648 ], [ -73.9814, 40.7681 ] 
      ] ] 
  } 
}, { 
  "type": "Feature", 
  "properties": {"style": "Blue", "name": "Blue"}, 
  "geometry": { 
      "type": "Polygon", 
      "coordinates":  [ [  
          // [ -73.958, 40.8003 ], [ -73.9498, 40.7968 ], [ -73.9737, 40.7648 ], [ -73.958, 40.8003 ] 
      ] ] 
  } 
}] 
}}



module.exports = router;

